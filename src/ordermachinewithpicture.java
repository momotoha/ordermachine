import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ordermachinewithpicture {
    private JPanel root;
    private JLabel toplabel;
    private JButton katudonButton;
    private JButton unadonButton;
    private JButton gyuudonButton;
    private JButton oyakodonButton;
    private JButton kaisendonButton;
    private JButton curryButton;
    private JTextPane receivedinfo;
    private JButton checkOutButton;
    private JLabel orderedinfo;
    private JLabel totalinfo;
    private JLabel costInfo;
    private JButton clearInfo;
    private JTextPane allergy;
    private JLabel allergyfield;


    int katudonprice=700;
    int Numberofkatudon=0;

    int oyakodonprice=800;
    int Numberofoyakodon=0;

    int kaisendonprice=900;
    int Numberofkaisendon=0;


    int unadonprice=1000;
    int Numberofunadon=0;

    int gyuudonprice=600;
    int Numberofgyuudon=0;

    int curryprice=500;
    int Numberofcurry=0;
    int total=0;

    int Numberofmenu=0;



    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order "+food+"?",
                "Order Confirmation",JOptionPane.YES_NO_OPTION);

        if(confirmation==0) {
            receivedinfo.setText("" +food+ "\n");
            JOptionPane.showMessageDialog(null,"Order for "+food+" received");

            if (food.equals("gyuudon")){
                if(Numberofgyuudon==0){
                    String currenttext = allergy.getText();
                    allergy.setText(currenttext+"gyuudon:wheal,beaf,soybean\n");
                    Numberofgyuudon++;

                    total+=gyuudonprice;

                }else{
                    total+=gyuudonprice;
                }
            } else if (food.equals("oyakodon")) {
                if(Numberofoyakodon==0){
                    String currernttext = allergy.getText();
                    allergy.setText(currernttext+"oyakodon:wheal,egg,chicken,soybean,milk\n");
                    Numberofoyakodon++;
                }else{
                }
            } else if (food.equals("unadon")) {
                if(Numberofunadon==0){
                    String currernttext = allergy.getText();
                    allergy.setText(currernttext+"unadon:wheal,seafood,soybean\n");
                    Numberofunadon++;
                    total+=unadonprice;
                }else{
                    total+=unadonprice;
                }
            } else if (food.equals("curry")) {
                if(Numberofcurry==0){
                    String currernttext = allergy.getText();
                    allergy.setText(currernttext+"curry:wheal,milk,beaf,soybean\n");
                    Numberofcurry++;
                    total+=curryprice;
                }else{
                    total+=curryprice;
                }
            } else if (food.equals("kaisendon")) {
                if(Numberofkaisendon==0){
                    String currernttext = allergy.getText();
                    allergy.setText(currernttext+"kaisendon:wheal,seafood,egg\n");
                    Numberofkaisendon++;
                    total+=kaisendonprice;
                }else{
                    total+=kaisendonprice;
                }
            }else if(food.equals("katudon")){
                if(Numberofkatudon==0){
                    String Currenttext = allergy.getText();
                    allergy.setText(Currenttext+"katudon:egg,wheal,milk,pork,soybean\n");
                    Numberofkatudon++;
                    total+=katudonprice;
                }else{
                    total+=katudonprice;
                }
            }

            costInfo.setText(total+" yen");

            Numberofmenu++;

        }

    }


    void order2(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order "+food+"?",
                "Order Confirmation",JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            String currernttext = receivedinfo.getText();
            receivedinfo.setText(currernttext + food + "\n");
            JOptionPane.showMessageDialog(null,"Order for "+food+" received");

            if (food.equals("gyuudon")){
                if(Numberofgyuudon==0){
                    String Currenttext = allergy.getText();
                    allergy.setText(Currenttext+"gyuudon:wheal,beaf,soybean\n");
                    total+=gyuudonprice;
                }else{
                    total+=gyuudonprice;
                }
            } else if (food.equals("oyakodon")) {
                if(Numberofoyakodon==0){
                    String Currernttext = allergy.getText();
                    allergy.setText(Currernttext+"oyakodon:wheal,egg,chicken,soybean,milk\n");
                    Numberofoyakodon++;
                    total+=oyakodonprice;
                }else{
                    total+=oyakodonprice;
                }
            } else if (food.equals("unadon")) {
                if(Numberofunadon==0){
                    String Currernttext = allergy.getText();
                    allergy.setText(Currernttext+"unadon:wheal,seafood,soybean\n");
                    Numberofunadon++;
                    total+=unadonprice;
                }else{
                    total+=unadonprice;
                }
            } else if (food.equals("curry")) {
                if(Numberofcurry==0){
                    String Currernttext = allergy.getText();
                    allergy.setText(Currernttext+"curry:wheal,milk,beaf,soybean\n");
                    Numberofcurry++;
                    total+=curryprice;
                }else{
                    total+=curryprice;
                }
            } else if (food.equals("kaisendon")) {
                if(Numberofkaisendon==0){
                    String Currernttext = allergy.getText();
                    allergy.setText(Currernttext+"kaisendon:wheal,seafood,egg\n");
                    Numberofkaisendon++;
                    total+=kaisendonprice;
                }else{
                    total+=kaisendonprice;
                }
            }else if(food.equals("katudon")){
                if(Numberofkatudon==0){
                    String Currenttext = allergy.getText();
                    allergy.setText(Currenttext+"katudon:egg,wheal,milk,pork,soybean\n");
                    Numberofkatudon++;
                    total+=katudonprice;
                }else{
                    total+=katudonprice;
                }
            }

            costInfo.setText(total+" yen");

            Numberofmenu++;

        }
    }

    public ordermachinewithpicture() {

        unadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("unagi2.png")
        ));

        gyuudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyudon2.png")
        ));

        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry2.png")
        ));

        kaisendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("kaisendon2.png")
        ));

        katudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("katudon2.png")
        ));

        oyakodonButton.setIcon(new ImageIcon(
                this.getClass().getResource("oyakodon2.png")
        ));


        katudonButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("katudon");
                }else{
                    order2("katudon");
                }

            }
        });


        unadonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("unadon");
                }else{
                    order2("unadon");
                }

            }
        });


        gyuudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("gyuudon");
                }else{
                    order2("gyuudon");
                }

            }
        });


        oyakodonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("oyakodon");
                }else{
                    order2("oyakodon");
                }

            }
        });


        kaisendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("kaisendon");
                }else{
                    order2("kaisendon");
                }

            }
        });


        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Numberofmenu==0){
                    order("curry");
                }else{
                    order2("curry");
                }

            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(Numberofmenu==0){
                    JOptionPane.showMessageDialog(null,"Nothing");
                }else {

                    int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?",
                            "Checkout Confirmation", JOptionPane.YES_NO_OPTION);

                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen");
                        receivedinfo.setText("");
                        costInfo.setText("0 yen");
                        allergy.setText("");
                        Numberofgyuudon=0; Numberofunadon=0; Numberofoyakodon=0; Numberofcurry=0; Numberofkaisendon=0; Numberofkatudon=0; Numberofmenu=0;
                        total=0;
                    }

                }



            }
        });


        clearInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Numberofgyuudon=0; Numberofunadon=0; Numberofoyakodon=0; Numberofcurry=0; Numberofkaisendon=0; Numberofkatudon=0;
                allergy.setText("");
                receivedinfo.setText("");
                costInfo.setText("0 yen");
                total=0;
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ordermachinewithpicture");
        frame.setContentPane(new ordermachinewithpicture().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
