import java.util.Scanner;

public class Sample2 {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.print("The number is: ");
        int num = userIn.nextInt();
        userIn.close();
        if (num > 10) {
            System.out.println("Large number.");
        } else {
            System.out.println("Small number.");
        }
    }
}